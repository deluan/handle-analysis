$LOAD_PATH.unshift(File.join(File.dirname(__FILE__), 'app/api'))
$LOAD_PATH.unshift(File.join(File.dirname(__FILE__), 'app/model'))

require 'handle_analysis'

run HandleAnalysis::API