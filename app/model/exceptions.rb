module HandleAnalysis
  module Exceptions

    class HandlerNotFound < StandardError
      attr_accessor :message

      def initialize(args={})
        @message = args[:message] || 'User not found'
      end
    end

  end
end

