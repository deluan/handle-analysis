require 'twitter'
require 'exceptions'

module HandleAnalysis
  class Analyzer
    MAX_LIMIT = 2000

    def get_top_hashtags(handler, limit)
      begin
        max_limit = [limit, MAX_LIMIT].min
        tweets = get_tweets(handler, max_limit)
        tweets.flat_map { |t| t.hashtags.map(&:text) }
            .group_by { |i| i }
            .map { |k, v| [k, v.length] }
            .sort_by { |k, v| v }
            .map { |k, v| k }
            .reverse
            .first(10)
      rescue Twitter::Error::NotFound => e
        raise Exceptions::HandlerNotFound.new(message: e.message)
      end
    end

    private

    def collect_with_max_id(collection=[], max_id=nil, &block)
      response = yield(max_id, collection.size)
      collection += response
      response.empty? ? collection.flatten : collect_with_max_id(collection, response.last.id - 1, &block)
    end

    def get_tweets(user, limit)
      collect_with_max_id do |max_id, total|
        if total < limit
          options = {count: [200, limit-total].min}
          options[:max_id] = max_id unless max_id.nil?
          client.user_timeline(user, options)
        else
          []
        end
      end
    end

    def client
      @client ||= Twitter::REST::Client.new do |config|
        config.consumer_key = ENV['TWITTER_CONSUMER_KEY']
        config.consumer_secret = ENV['TWITTER_CONSUMER_SECRET']
        config.access_token = ENV['TWITTER_ACCESS_TOKEN']
        config.access_token_secret = ENV['TWITTER_ACCESS_TOKEN_SECRET']
      end
    end
  end
end