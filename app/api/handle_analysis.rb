require 'grape'
require 'analyzer'
require 'exceptions'

module HandleAnalysis
  class API < Grape::API
    default_format :json

    rescue_from Exceptions::HandlerNotFound do |e|
      error!(e.message, 404)
    end

    get '/ping' do
      { ping: 'pong' }
    end

    params do
      requires :handle, type: String, desc: 'User handle (screen name)'
      requires :limit, type: Integer, desc: 'Max number of tweets to look back (capped at 2000)'
    end
    get '/handle-analysis' do
      Analyzer.new.get_top_hashtags(params[:handle], params[:limit])
    end

  end
end