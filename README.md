## Twitter Analyzer

This is an API in Ruby that accepts a Twitter handle (screen name) and a limit and responds with the top 10 hashtags
that the handle has ever tweeted. The limit is capped at a maximum of 2,000 tweets to look back.

## Usage

The project is currently deployed to Heroku. An example URL for testing it is:

https://nameless-springs-5111.herokuapp.com/handle-analysis?handle=google&limit=1000

## Development environment

This project uses Ruby 2.2.3. To set up your environment with RVM:

```
$ rvm install ruby-2.2.3
$ cd /path/to/project
$ gem install bundle
$ bundle install
```

You will need Twitter OAuth credentials. Create a copy of the file `.env.example` as `.env`, and put your credentials in it.

To start a local server, call `guard`. To run all specs just call `rspec`

__Important__: This project uses VCR to cache Twitter Rest API calls. If you change any tests or add new ones, remember
to remove all `Authentication` sections from the cassette at `spec/fixtures/vcr_cassettes/twitter-api.yml`. This should
probably be automated in a git pre-commit hook, to avoid any issues.
