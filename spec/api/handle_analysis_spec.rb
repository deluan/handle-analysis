require 'spec_helper'

describe HandleAnalysis::API do
  include Rack::Test::Methods

  def app
    HandleAnalysis::API
  end

  describe 'GET /ping' do
    it 'returns pong' do
      get '/ping'
      expect(last_response.status).to eq 200
      expect(last_response.body).to eq({ping: 'pong'}.to_json)
    end
  end


  describe 'GET /handle-analysis' do
    it 'returns a list of hashtags for the specified handle' do
      expect_any_instance_of(HandleAnalysis::Analyzer).to receive(:get_top_hashtags).with('deluan', 1000).and_return(['first', 'second', 'third'])

      get '/handle-analysis?handle=deluan&limit=1000'

      expect(last_response.status).to eq 200
      expect(last_response.body).to eq(['first', 'second', 'third'].to_json)
    end

    it 'returns bad request when handle is not specified' do
      get '/handle-analysis?limit=1000'
      expect(last_response.status).to eq 400
    end

    it 'returns bad request when limit is not specified' do
      get '/handle-analysis?handle=deluan'
      expect(last_response.status).to eq 400
    end

    it 'returns 404 on non-existent handler' do
      allow_any_instance_of(HandleAnalysis::Analyzer).to receive(:get_top_hashtags).and_raise(HandleAnalysis::Exceptions::HandlerNotFound)

      get '/handle-analysis?handle=non-existent&limit=1000'
      expect(last_response.status).to eq 404
    end
  end
end