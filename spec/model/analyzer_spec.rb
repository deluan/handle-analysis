require 'spec_helper'

vcr_options = {:cassette_name => 'twitter-api', :record => :once}

describe HandleAnalysis::Analyzer, :vcr => vcr_options do

  context 'get_top_hashtags' do
    let (:handler) { 'google' }
    let (:limit) { 200 }

    subject { HandleAnalysis::Analyzer.new.get_top_hashtags(handler, limit) }

    context 'when handle == google' do
      specify('should not return more than 10 hashtags') { subject.count <= 10 }

      context 'with limit == 200' do
        before { expect_any_instance_of(Twitter::REST::Client).to receive(:user_timeline).once.and_call_original }

        it 'should return the hashtags in the correct order' do
          expect(subject).to eq ['GlobalCandyCup', 'GoogleDoodle', 'GoogleApp', 'Nexus5X', 'OnHub', 'tech', 'Frightgeist', 'GoogleImpactChallenge', 'RockTheKasbah', 'GoogleMaps']
        end
      end

      context 'with limit == 400' do
        let (:limit) { 400 }

        before { expect_any_instance_of(Twitter::REST::Client).to receive(:user_timeline).twice.and_call_original }

        it 'should return the hashtags in the correct order' do
          expect(subject).to eq ['GlobalCandyCup', 'GoogleDoodle', 'GHC15', 'GoogleApp', 'GoogleImpactChallenge', 'Deutschland25', 'OnHub', 'Nexus5X', 'AndroidMarshmallow', 'GoogleGHC15']
        end
      end

      RESPONSE_FOR_LIMIT_2000 = ['GoogleTrends', 'OkGoogle', 'JustAsk', 'GoogleDoodle', 'io15', 'WWC2015', 'NatandLo', 'Oscars2015', 'MoreThanAMatch', 'Oscars']

      context 'with limit == 2000' do
        let (:limit) { 2000 }

        before { expect_any_instance_of(Twitter::REST::Client).to receive(:user_timeline).exactly(10).times.and_call_original }

        it 'should return the hashtags in the correct order' do
          expect(subject).to eq RESPONSE_FOR_LIMIT_2000
        end
      end

      context 'with limit == 3000' do
        let (:limit) { 3000 }

        before { expect_any_instance_of(Twitter::REST::Client).to receive(:user_timeline).exactly(10).times.and_call_original }

        it 'should cap the limit to 2000' do
          expect(subject).to eq RESPONSE_FOR_LIMIT_2000
        end
      end
    end

    context 'when handler is non-existent' do
      let (:handler) { 'dhjskfhasklDHSAklda' }

      it 'should raise an exception' do
        expect { subject.count }.to raise_error HandleAnalysis::Exceptions::HandlerNotFound
      end
    end
  end
end
