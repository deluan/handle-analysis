ENV['RACK_ENV'] ||= 'test'

require 'rubygems'
require 'rack/test'
require 'vcr'
require 'dotenv'

Dotenv.load

$LOAD_PATH.unshift(File.join(File.dirname(__FILE__), '../app/api'))
$LOAD_PATH.unshift(File.join(File.dirname(__FILE__), '../app/model'))

['app/api', 'app/model'].each do |dir|
  Dir[File.expand_path("../../#{dir}/*.rb", __FILE__)].each do |f|
    require f
  end
end

VCR.configure do |config|
  config.cassette_library_dir = 'spec/fixtures/vcr_cassettes'
  config.hook_into :webmock
  config.configure_rspec_metadata!
end

RSpec.configure do |config|
  config.mock_with :rspec
  config.expect_with :rspec
  config.raise_errors_for_deprecations!
end

